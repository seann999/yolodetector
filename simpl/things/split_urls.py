import os

if __name__ == "__main__":
    urls = open("winter11_urls.txt", "r")
    current_nid = -1
    current_file = None

    for line in urls:
        tokens = line.split()

        name = tokens[0]
        url = tokens[1]

        tokens = name.split("_")

        nid = tokens[0]
        num = tokens[1]

        if current_nid != nid:
            if current_file is not None:
                current_file.close()

            current_nid = nid
            current_file = open(os.path.join("urls", "%s.txt" % nid), "a+")

            print(nid)

        current_file.write("%s %s\n" % (name, url))

    urls.close()