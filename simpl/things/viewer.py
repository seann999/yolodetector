import sys
import cv2
import os
import xmltodict
import time
import csv
import pickle
import numpy as np
import random
from random import shuffle
import voc_loader

base_dir = "../things"
anno_dir = "../things/Annotation"

nid2id = {}
id2label = {}

def get_image(fn):
    pass


def get_nids():
    nids = []
    labels = []

    with open(os.path.join(base_dir, 'list.csv'), 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')

        for row in reader:
            nids.append(row[1])
            labels.append(row[0])

    return nids, labels

def yield_dataset(train=True):
    annos = get_dataset(train)

    while True:
        shuffle(annos)
        for f in annos:
            # read_xml(anno_dir + "/" + f, show=True)
            try:
                img, infos = read_xml2(anno_dir + "/" + f)

                if len(img.shape) == 3 and img.shape[2] == 3:
                    yield img, infos

            except:
                pass


def get_labeldict():
    nids, labels = get_nids()

    id2label = dict(zip(range(len(nids)), labels))
    return id2label

def get_dataset(train=True):
    #flickr_error = pickle.load(open(os.path.join(base_dir, "flickr_error.p"), "rb"))

    nids, labels = get_nids()

    global nid2id, id2label
    nid2id = dict(zip(nids, range(len(nids))))
    id2label = dict(zip(range(len(nids)), labels))
    print(id2label)
    #nid2id = dict({ for nid in get_nids()})

    def listdir(nid):
        try:
            a = os.listdir(os.path.join(base_dir, "Annotation", nid))
            return a
        except:
            print("no annotations for %s" % nid)
            return []

    annos = ["%s/%s" % (nid, fn) for nid in nids for fn in listdir(nid)]
    random.seed(7)
    shuffle(annos)
    print(len(annos))

    print(int(len(annos) * 0.7))

    if train:
        annos = annos[:int(len(annos) * 0.7)]
    else:
        annos = annos[int(len(annos) * 0.7):]

    random.seed(time.time())

    return annos

    #while True:
    #    files = os.listdir(anno_dir)
    #    shuffle(files)
    #    for f in files:
            # read_xml(anno_dir + "/" + f, show=True)
    #        yield read_xml(anno_dir + "/" + f, show=False)

def read_xml2(filepath):
    tokens = filepath.split("_")
    nid = tokens[0].split("/")[-1]

    return read_xml(filepath, nid)

def read_xml(filepath, nid):
    with open(filepath) as fd:
        doc = xmltodict.parse(fd.read())

    anno = doc["annotation"]
    fn = anno["filename"]
    objects = anno['object']

    infos = []

    #print(nid)
    #print(fn)
    img_path = os.path.join(base_dir, "images", "%s/%s.jpg" % (nid, fn))

    img, h, w, off_x, off_y = voc_loader.load_image(img_path)

    def get_info(o):
        bb = o["bndbox"]
        s = float(min(w, h))
        x1 = (float(bb["xmin"]) - w / 2.0) / s + 0.5
        y1 = (float(bb["ymin"]) - h / 2.0) / s + 0.5
        x2 = (float(bb["xmax"]) - w / 2.0) / s + 0.5
        y2 = (float(bb["ymax"]) - h / 2.0) / s + 0.5

        return [x1, y1, x2, y2, nid2id[nid]]

    if isinstance(objects, list):
        for obj in objects:
            infos.append(get_info(obj))
    else:
        infos.append(get_info(objects))


    #img = cv2.imread(img_path)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    return img, infos


if __name__ == "__main__":
    for img, infos in yield_dataset():
        print(infos)
        break

    """nid = sys.argv[1]

    flickr_error = pickle.load(open("flickr_error.p", "rb"))

    for f in os.listdir(os.path.join(base_dir, "Annotation", nid)):
        img, infos = read_xml(os.path.join(base_dir, "Annotation", nid, f), nid)

        if np.array_equal(img, flickr_error):
            print("skipping flickr 404")
            continue

        if img is None:
            print("image is none. skipping.")
            continue

        for info in infos:
            cv2.rectangle(img, (info[0], info[1]), (info[2], info[3]), (0, 255, 0), thickness=3)

        cv2.imshow("image", img)

        key = cv2.waitKey(0)

        if key == ord('q'):
            break
            #pickle.dump(img, open("flickr_error.p", "wb"))"""
