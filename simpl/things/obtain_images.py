import tarfile
import os
import xmltodict
import urllib
from urllib.parse import urlparse
from os.path import splitext
import csv
import time
import datetime
import socket
from threading import Thread

socket.setdefaulttimeout(10)

FAIL = 0
SUCCESS = 1
SKIP = 2

def extract_tar(nid):
    fname = os.path.join("Annotation", "%s.tar.gz" % nid)

    if not os.path.exists(fname):
        return False

    tar = tarfile.open(fname, "r:gz")
    tar.extractall()
    tar.close()

    return True

def read_xml(filename):
    with open(filename) as fd:
        doc = xmltodict.parse(fd.read())

    anno = doc["annotation"]
    fn = anno["filename"]

    return fn

def download_image(dir, name, url):
    try:
        parsed = urlparse(url)
        root, ext = splitext(parsed.path)
        filepath = os.path.join(dir, "%s%s" % (name, ext))

        if not os.path.exists(filepath):
            urllib.request.urlretrieve(url, filepath)
            return SUCCESS
        else:
            return SKIP
    except Exception as e:
        log("DOWNLOAD FAILED: %s %s" % (url, e))
        return FAIL

def download_urls(nid, pairs):
    dir = os.path.join("images", nid)

    if not os.path.exists(dir):
        os.makedirs(dir)

    status = [0, 0, 0]

    for name, url in pairs:
        re = download_image(dir, name, url)

        status[re] += 1

    return status

def begin_downloads(nid):
    def split(s):
        tokens = s.split()
        return tokens[0], tokens[1]

    with open(os.path.join("urls", "%s.txt" % nid)) as f:
        name2url = dict(split(line) for line in f.readlines())

    pairs = []

    for f in os.listdir(os.path.join("Annotation", nid)):
        fn = read_xml(os.path.join("Annotation", nid, f))

        if fn in name2url:
            pairs.append((fn, name2url[fn]))
        else:
            pass
            #print("%s not found in bounding box data." % fn)

    return download_urls(nid, pairs)

def log(msg):
    with open('log.txt', 'a+') as logfile:
        st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

        logfile.write("[%s] " % st)
        logfile.write("%s\n" % msg)
    print(msg)

def process_row(row):
    str_label = row[0]
    nid = row[1]

    log("BEGINNING DOWNLOAD FOR: %s(%s)" % (nid, str_label))

    extract_success = extract_tar(nid)

    if not extract_success:
        log("Failed to extract tar for %s. Data probably absent." % nid)
        return

    status = begin_downloads(nid)
    status_str = "%i FAILURES, %i SUCCESSES, %i SKIPS" % tuple(status)

    log("STATUS FOR: %s(%s): %s" % (nid, str_label, status_str))

if __name__ == "__main__":
    with open('list.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        threads = []

        for row in reader:
            t = Thread(target=process_row, args=(row,))
            t.start()

            threads.append(t)

            if len(threads) >= 3:
                print("Waiting for threads to finish...")

                for t in threads:
                    t.join()

                print("All joined. Resuming.")

                threads = []
