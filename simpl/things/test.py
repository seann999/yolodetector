import os
import sys
import cv2
import os
import xmltodict
import csv
import pickle
import numpy as np
import random
from random import shuffle


base_dir = "../things"

def get_nids():
    nids = []
    print("43")

    with open(os.path.join(base_dir, 'list.csv'), 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')

        for row in reader:
            nids.append(row[1])

    print(nids)

    return nids


def read_dataset():
    #flickr_error = pickle.load(open(os.path.join(base_dir, "flickr_error.p"), "rb"))
    print("43")
    nids = get_nids()

    def listdir(nid):
        try:
            a = os.listdir(os.path.join(base_dir, "Annotation", nid))
            return a
        except:
            print("no annotations for %s" % nid)
            return []

    print(nids)

    annos = [fn for nid in nids for fn in listdir(nid)]
    random.seed(7)
    shuffle(annos)

    print(int(len(annos) * 0.7))

    if train:
        annos = annos[:int(len(annos) * 0.7)]
    else:
        annos = annos[int(len(annos) * 0.7):]

    print(annos)

    while True:
        files = os.listdir(anno_dir)
        shuffle(files)
        for f in files:
            # read_xml(anno_dir + "/" + f, show=True)
            yield read_xml(anno_dir + "/" + f, show=False)


def read_xml(filepath, nid):
    with open(filepath) as fd:
        doc = xmltodict.parse(fd.read())

    anno = doc["annotation"]
    fn = anno["filename"]
    objects = anno['object']

    infos = []

    def get_info(o):
        bb = o["bndbox"]
        x1 = int(bb["xmin"])
        y1 = int(bb["ymin"])
        x2 = int(bb["xmax"])
        y2 = int(bb["ymax"])

        return [x1, y1, x2, y2]

    if isinstance(objects, list):
        for obj in objects:
            infos.append(get_info(obj))
    else:
        infos.append(get_info(objects))

    img_path = os.path.join(base_dir, "images", nid, "%s.jpg" % fn)
    img = cv2.imread(img_path)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    return img, infos

if __name__ == "__main__":
    get_nids()
    #read_dataset()