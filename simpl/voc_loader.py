import xml.etree.ElementTree
import xmltodict
import cv2
import skimage
import skimage.io
import skimage.transform
import os
from random import shuffle

e = None
anno_dir = "/media/sean/Transcend/VOCdevkit/VOC2012/Annotations"
filename = anno_dir + "/2007_000032.xml"

root = "/media/sean/Transcend/VOCdevkit/VOC2012/JPEGImages/"

classes_i = ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
            "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]
classes = {classes_i[i]:i for i in range(len(classes_i))}

def read_dataset():
    while True:
        files = os.listdir(anno_dir)
        shuffle(files)
        for f in files:
            #read_xml(anno_dir + "/" + f, show=True)
            yield read_xml(anno_dir + "/" + f, show=False)

def load_image(path, size=224):
    img = skimage.io.imread(path)

    return crop(img, size)

def crop(img, size=224):
    o_h, o_w = img.shape[:2]

    short_edge = min(img.shape[:2])
    yy = int((img.shape[0] - short_edge) / 2)
    xx = int((img.shape[1] - short_edge) / 2)
    crop_img = img[yy:yy + short_edge, xx:xx + short_edge]
    resized_img = skimage.transform.resize(crop_img, (size, size))

    off_x = -(o_w - short_edge) / 2.0 / float(o_w)
    off_y = -(o_h - short_edge) / 2.0 / float(o_h)

    return resized_img, o_h, o_w, off_x, off_y

def read_xml(filename, show=False):
    with open(filename) as fd:
        doc = xmltodict.parse(fd.read())

    anno = doc["annotation"]
    path = root + anno["filename"]

    objects = anno['object']

    if show:
        img = cv2.imread(path)

        def draw_rect(obj):
            bb = obj["bndbox"]
            x1 = int(bb["xmin"])
            y1 = int(bb["ymin"])
            x2 = int(bb["xmax"])
            y2 = int(bb["ymax"])
            cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 0), thickness=2)
            text = obj["name"]
            text += "(%i)" % classes[text]
            cv2.putText(img, text, (x1 + 3, y1 + 12), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))

        if isinstance(objects, list):
            for obj in objects:
                draw_rect(obj)
        else:
            draw_rect(objects)

        cv2.imshow("image", img)

        #cv2.waitKey(0)
    else:
        img, h, w, off_x, off_y = load_image(path)

        def extract(obj, h, w):
            bb = obj["bndbox"]
            s = float(min(w, h))
            x1 = (float(bb["xmin"]) - w / 2.0) / s + 0.5
            y1 = (float(bb["ymin"]) - h / 2.0) / s + 0.5
            x2 = (float(bb["xmax"]) - w / 2.0) / s + 0.5
            y2 = (float(bb["ymax"]) - h / 2.0) / s + 0.5
            text = obj["name"]
            #text += "(%i)" % classes[text]
            return [x1, y1, x2, y2, classes[text]]

        object_list = []

        if isinstance(objects, list):
            for obj in objects:
                object_list.append(extract(obj, h, w))
        else:
            object_list.append(extract(objects, h, w))

        #img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        return img, object_list

def show_dataset():
    for f in os.listdir(anno_dir):
        read_xml(anno_dir + "/" + f, show=True)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    e = xml.etree.ElementTree.parse(filename).getroot()
    #parse_dataset()
    for img, info in read_dataset():
        print(info)
