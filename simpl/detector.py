import tensorflow as tf
import numpy as np
import model
import vars
import cv2
import sys
import time
import voc_loader
import nms
import things.viewer
import traceback
import augmenter
import pickle
import chopper

label_dict = None



def get_data(dataset, aug=True):
    #dataset = voc_loader.read_dataset()


    while True:
        imgs = []
        infos = []
        try:
            for i in range(vars.batch_size):
                success = False

                while not success:
                    img, info = dataset.__next__()
                    #img = scipy.misc.imresize(img, (vars.image_h, vars.image_w))

                    if label_dict is None:
                        global label_dict
                        label_dict = things.viewer.id2label

                    if aug:
                        img, info = augmenter.augment(img, info)

                    imgs.append(img)
                    infos.append(info)
                    success = True

            yield imgs, infos
        except Exception as e:
            print(e)
            print("generator error")
            traceback.print_exc()
            break

def calc_iou(box1, box2):
    x5 = np.max(box1[0] - box2[0])
    y5 = np.max(box1[1] - box2[1])
    x6 = np.min(box1[2] - box2[2])
    y6 = np.min(box1[3] - box2[3])

    inter = 0

    if x5 < x6 and y5 < y6:
        inter = abs(x5 - x6) * abs(y5 * y6)
    union = abs(box1[0] - box1[2]) * abs(box1[1] - box1[3]) + abs(box2[0] - box2[2]) * abs(box2[1] - box2[3]) - inter
    return inter / union

def show_grid(img, infos, labels=None, boxes=None, regr_truth=None, resp=None):
    img = cv2.resize(img, (img.shape[0] * 4, img.shape[0] * 4))
    scale_text = 3.0
    line_width = 5
    w = img.shape[0]

    img = (img * 255.0).astype(np.uint8)

    unit = w // vars.grid_size

    top_labels, max_labels = None, None
    superlabel = None
    superlabels2 = None

    if labels is not None:
        superlabel = np.argmax(np.sum(labels, axis=(0, 1)))
        superlabels2 = np.sum(labels * np.expand_dims(np.max(boxes[:,:,4::5], axis=2), 2), axis=(0, 1)).argsort()[-5:][::-1]
        top_labels = np.argmax(labels, axis=2)
        max_labels = np.max(np.exp(labels), axis=2) / np.sum(np.exp(labels), axis=2)

    if infos is not None:
        for info in infos:
            coords = [int(i * w) for i in info[:4]]
            cv2.rectangle(img, (coords[0], coords[1]), (coords[2], coords[3]), color=(0, 255, 0), thickness=line_width*2)
            cv2.putText(img, label_dict[info[4]],
                        (coords[0], coords[3]), cv2.FONT_HERSHEY_PLAIN, scale_text, (0, 255, 0), thickness = 3)

    count = 0

    c_thresh = 0.1
    b = np.reshape(boxes, (2*(vars.grid_size**2), 5))
    #b = b.tolist()
    aa = np.reshape(max_labels, (vars.grid_size * vars.grid_size))
    #print(b)
    ii = [i for i in range(len(b)) if b[i, 4] * aa[i // vars.b] > c_thresh]
    o_b = b[ii]
    b = [box[:4] for box in o_b]
    b = np.asarray(b)

    picks = nms.non_max_suppression_fast(b, 0.5)
    p = np.zeros((vars.grid_size*vars.grid_size*vars.b))

    ii = [ii[i] for i in picks]
    p[ii] = 1
    p = np.reshape(p, (vars.grid_size, vars.grid_size, vars.b))

    #cv2.putText(img, label_dict[superlabels2[0]],
    #            (10, w), cv2.FONT_HERSHEY_PLAIN, scale_text,
    #            color=(255, 0, 255), thickness=3)
    #for iii in range(1, len(superlabels2) + 1):
    #    cv2.putText(img, label_dict[superlabels2[-iii]],
    #                (10, w - 50 * iii), cv2.FONT_HERSHEY_PLAIN, scale_text,
    #                color=(255, 0, 255), thickness=3)

    #cv2.putText(img, label_dict[superlabel],
    #            (10, w - 10), cv2.FONT_HERSHEY_PLAIN, scale_text,
    #            color=(255, 0, 255), thickness = 3)

    for x in range(vars.grid_size):
        for y in range(vars.grid_size):

            if boxes is not None:
                for bi in range(int(boxes[y, x].shape[0] / 5)):
                    count += 1

                    bx, by, bw, bh, conf = boxes[y, x][bi*5:(bi*5+5)]

                    regr = None

                    if regr_truth is not None:
                        regr = regr_truth[y, x][bi*5:(bi*5+5)]

                    confidence = conf * max_labels[y, x]
                    confident = confidence >= c_thresh and p[y,x, bi] == 1
                    confident_t = (regr is not None and regr[4] == 1)

                    if confident:# or confident_t:
                        line_width = int(confidence * 10.0)
                        print("box %i" % count)

                        print(str(x) + " " + str(y))
                        print(regr)
                        print("confidence: %f" % (conf * max_labels[y, x]))
                        print("pred: %s" % boxes[y, x][bi*5:(bi*5+5)])

                        if resp is not None:
                            print("resp: %s" % resp[y, x][bi*5:(bi*5+5)])

                        print("##")

                        bxx, byy = bx - bw / 2.0, by - bh / 2.0
                        bxx2, byy2 = bxx + bw, byy + bh

                        col = (255, 0, 0)

                        if confident_t and not confident:
                            col = (255, 128, 0)

                        if confident and confident_t:
                            col = (0, 255, 255)

                        cv2.rectangle(img, (int(bxx * w), int(byy * w)),
                                      (int(bxx2 * w), int(byy2 * w)), color=col, thickness=line_width)

                        #if confident_t:
                        #    dx = regr[0] - regr[2] / 2.0
                        #    dy = regr[1] - regr[3] / 2.0
                        #    dx2 = dx + regr[2]
                        #    dy2 = dy + regr[3]

                        #    cv2.rectangle(img, (int(dx * w),
                        #                        int(dy * w)),
                        #                  (int((dx2) * w),
                        #                   int((dy2) * w)),
                        #                  color=(0, 0, 255), thickness=line_width)

                        cv2.putText(img, label_dict[top_labels[y][x]],
                                    (int(bxx * w), int((byy * w + byy2 * w) / 2.0)), cv2.FONT_HERSHEY_PLAIN, scale_text,
                                    col, thickness = 2)

    img_grid = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    #img_grid = (img_grid*255.0).astype(np.uint8)
    cv2.namedWindow("image grid", cv2.WINDOW_NORMAL)
    cv2.imshow("image grid", img_grid)

    return img_grid
    #cv2.waitKey(1)

def train():
    import things
    test_dataset = get_data(things.viewer.yield_dataset(train=False), aug=False)

    with tf.Graph().as_default():
        sess = tf.Session()

        m = model.Model()
        train_phase = tf.placeholder(tf.bool)

        x, features, [output_c, output_r], [thing_t, class_t, reg_t, resp_t], loss, train_op, acc, g_step = m.inference(sess, train_phase)

        init = tf.initialize_all_variables()

        ckpt = tf.train.get_checkpoint_state("summaries2/%s" % sys.argv[2])
        saver = tf.train.Saver()
        summary_writer = tf.train.SummaryWriter("summaries2/%s" % sys.argv[2])

        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            print("restored")
        else:
            sess.run(init)

        print("starting training...")

        for imgs, infos in get_data(things.viewer.yield_dataset()):
            #print("batch loaded.")

            imgs = np.asarray(imgs)

            #infos_one = [info[0][4] for info in infos]
            #infos_one = np.asarray(infos_one)
            mat_info = [info2mat(info) for info in infos]
            things = np.asarray([i[0] for i in mat_info])
            classes = np.asarray([i[1] for i in mat_info])

            fd = {x: imgs,
                  train_phase: True}
            #print("updating weights...")
            #output_f, output_rf, loss_f, _, acc_f, step = sess.run([output_c, output_r, loss, train_op, acc, g_step], fd)
            output_f, output_rf, step = sess.run([output_c, output_r, g_step],
                                                                   fd)

            regr_truths = []
            resp_mats = []

            for i in range(len(infos)):
                regr_truth, resp_mat = calc_responsibility(infos[i], output_rf[i])
                regr_truths.append(regr_truth)
                resp_mats.append(resp_mat)

            fd = {x: imgs,
                  thing_t: things,
                  class_t: classes,
                  train_phase: True,
                  reg_t: regr_truths,
                  resp_t: resp_mats
                  }

            loss_f, _, acc_f, step = sess.run([loss, train_op, acc, g_step],
                                                                   fd)

            summary = tf.Summary(
                value=[tf.Summary.Value(tag="loss", simple_value=float(loss_f))])
            summary_writer.add_summary(summary, global_step=step)

            summary = tf.Summary(
                value=[tf.Summary.Value(tag="accuracy", simple_value=float(acc_f))])
            summary_writer.add_summary(summary, global_step=step)

            summary_writer.flush()

            show_grid(imgs[0], infos[0], output_f[0], output_rf[0], regr_truths[0], resp_mats[0])

            if step % 100 == 0 or step == 1:
                print("evaluating...")

                loss_t = 0
                acc_t = 0

                for _ in range(10):
                    imgs_test, infos_test = test_dataset.__next__()
                    imgs_test = np.asarray(imgs_test)

                    #print([nn.shape for nn in imgs_test])
                    #print([len(nn) for nn in infos_test])
                    mat_info2 = [info2mat(info) for info in infos_test]
                    things2 = np.asarray([i[0] for i in mat_info2])
                    classes2 = np.asarray([i[1] for i in mat_info2])


                    fd = {x: imgs_test,
                          train_phase: False}

                    output_f_test, output_rf_test = sess.run([output_c, output_r],
                                                   fd)
                    regr_truths = []
                    resp_mats = []

                    for i in range(len(infos)):
                        regr_truth, resp_mat = calc_responsibility(infos_test[i], output_rf_test[i])
                        regr_truths.append(regr_truth)
                        resp_mats.append(resp_mat)

                    fd = {x: imgs_test,
                          thing_t: things2,
                          class_t: classes2,
                          train_phase: False,
                          reg_t: regr_truths,
                          resp_t: resp_mats
                          }

                    loss_f, acc_f = sess.run([loss, acc],
                                                      fd)
                    loss_t += loss_f
                    acc_t += acc_f

                loss_t /= 10.0
                acc_t /= 10.0
                summary = tf.Summary(
                    value=[tf.Summary.Value(tag="eval loss", simple_value=float(loss_t))])
                summary_writer.add_summary(summary, global_step=step)
                summary = tf.Summary(
                    value=[tf.Summary.Value(tag="eval accuracy", simple_value=float(acc_t))])
                summary_writer.add_summary(summary, global_step=step)

                summary_writer.flush()

            if step % 500 == 0:
                saver.save(sess, "summaries2/%s/model" % (sys.argv[2]), global_step=step)

            #print("weights updated.")

            print("="*32)
            print("step: %i" % step)
            #print("truth: %s" % infos)
            #print("preds: %s" % np.argmax(output_f, axis=1))
            print("loss: %f" % loss_f)
            print("accuracy: %f" % acc_f)

            #print("loading batch...")


            #cv2.imshow("image", cv2.cvtColor(imgs[0].astype(np.float32), cv2.COLOR_RGB2BGR))
            cv2.waitKey(1)

def calc_responsibility(infos, output):

    resp = np.zeros((vars.grid_size, vars.grid_size, vars.b * 5))
    r_truth = np.zeros((vars.grid_size, vars.grid_size, vars.b * 5))

    resp[:, :, 4::5] = 0.5

    for info in infos:
        x1, y1, x2, y2, c = info

        xx = int((x1 + x2) * vars.grid_size / 2.0)
        yy = int((y1 + y2) * vars.grid_size / 2.0)

        if xx >= 0 and yy >= 0 and xx < vars.grid_size and yy < vars.grid_size:
            outs = output[yy, xx]

            resp_i = -1
            max_iou = 0

            for i in range(int(len(outs) / 5)):
                bx, by, bw, bh, _ = outs[i*5:(i+1)*5]
                bx = bx - bw / 2.0
                by = by - bh / 2.0
                bx2 = bx + bw
                by2 = by + bh

                iou = calc_iou([bx, by,
                                bx2, by2], [x1, y1, x2, y2])

                if iou > max_iou:
                    max_iou = iou
                    resp_i = i

            if resp_i == -1:
                min_dist = 10000
                tcx = (x1 + x2) / 2.0
                tcy = (y1 + y2) / 2.0

                for i in range(int(len(outs) / 5)):
                    bx, by, bw, bh, _ = outs[i * 5:(i + 1) * 5]
                    bx = bx - bw / 2.0
                    by = by - bh / 2.0
                    cx = bx + bw / 2.0
                    cy = by + bh / 2.0
                    d = ((cx - tcx)**2 + (cy - tcy)**2)
                    if d < min_dist:
                        min_dist = d
                        resp_i = i

            if resp_i != -1:

                cx = (x1 + x2) / 2.0
                cy = (y1 + y2) / 2.0
                r_truth[yy, xx, resp_i  * 5:resp_i *5 + 5] = cx, cy, x2-x1, y2-y1, 1
                resp[yy, xx, resp_i * 5:resp_i * 5 + 5] = 5, 5, 5, 5, 1

    return r_truth, resp

def info2mat(infos):
    thing = np.zeros([vars.grid_size, vars.grid_size])
    class_ = np.zeros([vars.grid_size, vars.grid_size])

    for info in infos:
        x1, y1, x2, y2, c = info

        xx = int((x1 + x2) * vars.grid_size / 2.0)
        yy = int((y1 + y2) * vars.grid_size / 2.0)

        if xx >= 0 and yy >= 0 and xx < vars.grid_size and yy < vars.grid_size:
            thing[yy, xx] = 1
            class_[yy, xx] = c

    return thing, class_

def view_data():
    test_dataset = get_data(things.viewer.yield_dataset(train=True), aug=False)
    for imgs, infos in get_data(test_dataset):
        show_grid(imgs[-1], infos[-1])
        #cv2.imshow("image", cv2.cvtColor(imgs[0].astype(np.float32), cv2.COLOR_RGB2BGR))
        cv2.waitKey(0)

def initialize(sess):
    global label_dict
    label_dict = things.viewer.get_labeldict()

    m = model.Model()
    train_phase = tf.placeholder(tf.bool)

    x, features, [output_c, output_r], [thing_t, class_t, reg_t, resp_t], loss, train_op, acc, g_step = m.inference(
        sess, train_phase)

    init = tf.initialize_all_variables()

    ckpt = tf.train.get_checkpoint_state("summaries2/%s" % sys.argv[2])
    saver = tf.train.Saver()

    if ckpt and ckpt.model_checkpoint_path:
        saver.restore(sess, ckpt.model_checkpoint_path)
        print("restored")
    else:
        quit()

    return x, train_phase, output_c, output_r

def evaluate_img(path):
    imgs = chopper.process_image(path)

    #img, _, _, _, _ = voc_loader.load_image(path, size=224)

    with tf.Graph().as_default():
        sess = tf.Session()



        input_img, train_phase, output_c, output_r = initialize(sess)
        #img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        #img = voc_loader.crop(img)[0]
        print("begin")
        boxes = []

        index = 0
        batch_size = 1

        for i in range(0, len(imgs), batch_size):
            print("%i of %i" % (index, len(imgs)))
            index += batch_size

            batch = []
            images = []
            sizes = []
            for img, imgx, imgy, imgx2, imgy2 in imgs[i:i+batch_size]:
                images.append(img)
                img = voc_loader.crop(img)[0]
                batch.append(img)
                sizes.append([imgx, imgy, imgx2, imgy2])

            fd = {input_img: batch,
                  train_phase: False}
            output_f_batch, output_rf_batch = sess.run([output_c, output_r],
                                           fd)

            for i in range(batch_size):
                img_h, img_w = images[i].shape[:2]
                output_f, output_rf = output_f_batch[i], output_rf_batch[i]
                scale = img_h

                #probs = np.exp(output_f) / np.expand_dims(np.sum(np.exp(output_f), axis=2), axis=2)
                max_labels = np.max(np.exp(output_f), axis=2) / np.sum(np.exp(output_f), axis=2)
                for x in range(vars.grid_size):
                    for y in range(vars.grid_size):
                        for bi in range(int(output_rf[y, x].shape[0] / 5)):
                            bx, by, bw, bh, conf = output_rf[y, x][bi * 5:(bi * 5 + 5)]
                            probs = np.exp(output_f[y][x]) / np.sum(np.exp(output_f[y][x]))
                            fin_conf = conf * max_labels[y, x]


                            bw *= scale
                            bh *= scale
                            bxx, byy = bx * scale - bw / 2.0 + sizes[i][0], by * scale - bh / 2.0 + sizes[i][1]
                            bxx2, byy2 = bxx + bw, byy + bh

                            #bxx = bxx * img_w + imgx
                            #bxx2 = bxx2 * img_w + imgx
                            #byy = byy * img_h + imgy
                            #byy2 = byy2 * img_h + imgy

                            topargs = output_f[y][x].argsort()[-5:][::-1]
                            probs = probs[topargs]

                            boxes.append([fin_conf, conf, topargs, probs, bxx, bxx2, byy, byy2])

                #show_grid(img, None, output_f, output_rf)
                #cv2.waitKey(0)

        pickle.dump([path, boxes], open("%s.p" % sys.argv[3], "wb"))

        visualize()

def allow_label(lab):
    if "cotton" in lab:
        return False
    return True

def visualize():
    global label_dict
    label_dict = things.viewer.get_labeldict()
    path, boxes = pickle.load(open("%s.p" % sys.argv[3], "rb"))

    fimg = cv2.imread(path)
    #img = np.rot90(fimg)
    #img = np.rot90(img)
    #fimg = np.rot90(img).copy()

    conf_thresh = float(sys.argv[4])
    conf_thresh2 = float(sys.argv[5])
    search = None

    if len(sys.argv) == 7:
        search = str(sys.argv[6])
        print("searching for %s" % search)

    print(fimg.shape)

    #maxbox = np.argmax(np.asarray([b[4] for b in boxes]))

    for b in range(len(boxes)):
        box = boxes[b]
        fin_conf, conf, topargs, probs, x1, x2, y1, y2 = box
        #fin_conf >= conf_thresh
        if conf >= conf_thresh2 and probs[0] > conf_thresh and allow_label(label_dict[topargs[0]]): # and conf > 0.9
            if search is None:
                cv2.rectangle(fimg, (int(x1), int(y1)), (int(x2), int(y2)),
                              color=(0, 255, 0), thickness=3)

                cv2.putText(fimg, label_dict[topargs[0]],
                            (int(x1), int(y2)), cv2.FONT_HERSHEY_PLAIN, 5,
                            color=(0, 255, 0), thickness=5)
            else:
                hit = False
                hit_word = ""

                for top in topargs[-5:]:
                    if search[-1] == "!":
                        if search[:-1] in label_dict[top].split() or search[:-1] in label_dict[top].split(","):
                            print(label_dict[top])
                            hit = True
                            hit_word = label_dict[top]
                    else:
                        if search in label_dict[top]:
                            hit = True
                            hit_word = label_dict[top]

                if hit:
                    cv2.rectangle(fimg, (int(x1), int(y1)), (int(x2), int(y2)),
                                  color=(0, 255, 0), thickness=3)

                    cv2.putText(fimg, hit_word,
                                (int(x1), int(y2)), cv2.FONT_HERSHEY_PLAIN, 5,
                                color=(0, 255, 0), thickness=5)

    cv2.namedWindow("final", cv2.WINDOW_NORMAL)
    cv2.imshow("final", fimg)
    cv2.imwrite("%s.results.png" % sys.argv[3], fimg)
    cv2.waitKey(0)

def evaluate_webcam():
    import webcam
    wc = webcam.WebcamStream()
    wc.start_stream_threads()

    out = cv2.VideoWriter(filename='output.avi', fourcc=cv2.VideoWriter_fourcc(*'MJPG'), fps=10.0,
                                  frameSize=(896, 896))

    with tf.Graph().as_default():
        sess = tf.Session()

        x, train_phase, output_c, output_r = initialize(sess)

        while(True):
            print(time.time())
            if wc.image is not None:
                img = cv2.cvtColor(wc.image, cv2.COLOR_RGB2BGR)
                img = voc_loader.crop(img)[0]
                fd = {x: [img],
                      train_phase: False}

                output_f, output_rf = sess.run([output_c, output_r],
                                               fd)

                img = show_grid(img, None, output_f[0], output_rf[0])

                out.write(img)

                cv2.waitKey(50)
            else:
                #pass
                print("image is none")

    out.release()

if __name__ == "__main__":
    if sys.argv[1] == "train":
        train()
    elif sys.argv[1] == "test":
        evaluate_webcam()
    elif sys.argv[1] == "image":
        evaluate_img(sys.argv[3])
    elif sys.argv[1] == "vis":
        visualize()
    elif sys.argv[1] == "view":
        view_data()
