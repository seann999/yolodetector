import skimage.io
import skimage.transform
import sys
import scipy.misc
import cv2
import numpy as np

def crop_patches(img, stride, size):
    patches = []
    h, w = img.shape[:2]

    for y in range(0, h - size, stride):
        for x in range(0, w - size, stride):
            patch = img[y:y+size, x:x+size]
            patches.append([patch, x, y, x + size, y + size])

    return patches

def process_image(path):
    #img = skimage.io.imread(path)
    img = cv2.imread(path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    #img = np.rot90(img)
    #img = np.rot90(img)
    #img = np.rot90(img)

    images = []

    scale = int(min(img.shape[:2]) / 128)
    #scale = max(0, scale)

    for i in range(1, 2):
        s = 128 * (2**i)
        s2 = int(s / 4.0)
        print(s)
        patches = crop_patches(img, s2, s)
        images.extend(patches)

    #print(len(images))

    for i in range(len(images)):
        if i < 100:
            scipy.misc.imsave('patches/%i.jpg' % i, images[i][0])

    return images

if __name__ == "__main__":
    process_image(sys.argv[1])