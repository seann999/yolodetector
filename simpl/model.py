import tensorflow as tf
import cnn_model
from tensorflow.python.training import moving_averages
import vars
import numpy as np
import numpy as np

xi = tf.contrib.layers.xavier_initializer

def batch_norm(x, phase_train, scope_name='bn'):
    """
    Batch normalization on convolutional maps.
    Args:
        x:           Tensor, 4D BHWD input maps
        n_out:       integer, depth of input maps
        phase_train: boolean tf.Varialbe, true indicates training phase
        scope:       string, variable scope
    Return:
        normed:      batch-normalized maps
    """

    with tf.variable_scope(scope_name) as scope:
        inputs_shape = x.get_shape().dims
        axes = list(range(len(inputs_shape) - 1))
        n_out = int(inputs_shape[-1])

        beta = tf.get_variable(name='beta', shape=[n_out], initializer=tf.constant_initializer(0))
        gamma = tf.get_variable(name='gamma', shape=[n_out], initializer=tf.constant_initializer(1))

        moving_mean = tf.get_variable(name='moving_mean', shape=[n_out], initializer=tf.constant_initializer(0),
                                      trainable=False)
        moving_variance = tf.get_variable(name='moving_variance', shape=[n_out], initializer=tf.constant_initializer(1),
                                          trainable=False)

        mean, variance = tf.nn.moments(x, axes, name='moments')

        decay = 0.999

        update_moving_mean = moving_averages.assign_moving_average(
            moving_mean, mean, decay)

        update_moving_variance = moving_averages.assign_moving_average(
            moving_variance, variance, decay)

        def mean_var_with_update():  # values to use during training
            with tf.control_dependencies([update_moving_mean, update_moving_variance]):
                return tf.identity(mean), tf.identity(variance)

        mean, var = tf.cond(phase_train,
                            mean_var_with_update,
                            lambda: (moving_mean, moving_variance))

        normed = tf.nn.batch_normalization(x, mean, var, beta, gamma, 1e-3)

        scope.reuse_variables()

    return normed

def xwb(name, x, _in, out, train, use_bn=True):
    with tf.variable_scope(name):
        w = tf.get_variable(name="w", shape=[_in, out], initializer=xi())

        xw = tf.matmul(x, w)

        if use_bn:
            re = batch_norm(xw, train)
        else:
            b = tf.get_variable(name="b", shape=[out], initializer=tf.constant_initializer(0))
            re = xw + b
    return re

def predict(features, train):
    flat_d = features.get_shape()[1].value * features.get_shape()[2].value * features.get_shape()[3].value
    features = tf.reshape(features, [vars.batch_size_dec, flat_d])

    hidden_n = 2048

    hidden = tf.nn.relu(xwb("hidden", features, flat_d, hidden_n, train, use_bn=True))

    class_output = xwb("output_flat", hidden, hidden_n, vars.grid_size * vars.grid_size * vars.classes, train, use_bn=True)

    class_box = tf.reshape(class_output, [vars.batch_size_dec, vars.grid_size, vars.grid_size, vars.classes])

    regr_output = tf.sigmoid(xwb("output_r_flat", hidden, hidden_n, vars.grid_size * vars.grid_size * vars.b * 5, train, use_bn=True))

    regr_box = tf.reshape(regr_output, [vars.batch_size_dec, vars.grid_size, vars.grid_size, vars.b * 5])

    #classify = xwb("classify", x, flat_d, vars.classes)
    #regress = xwb("regress", x, flat_d, 4)

    return class_box, regr_box

def trainer(output, output_r, things, classes, resp, reg_t):

    #regression = output[:,:,:,-5:]
    truth_c = tf.cast(classes, tf.int32)

    output_c = output

    truth_c = tf.reshape(truth_c, [-1])
    output_c = tf.reshape(output_c, [-1, vars.classes])
    truth_t = tf.reshape(things, [-1])

    softmax_loss = tf.nn.sparse_softmax_cross_entropy_with_logits(output_c, truth_c)

    pows = np.ones([vars.batch_size, vars.grid_size, vars.grid_size, vars.b * 5])
    pows[:,:,:,2::5] = 0.5
    pows[:,:,:,3::5] = 0.5
    output_r = tf.pow(output_r, pows)
    reg_t = tf.pow(reg_t, pows)

    regr_loss = tf.pow(output_r - reg_t, 2)

    #resp = tf.reshape(resp, [-1, 1])
    #resp = tf.tile(resp, [1, 5])
    #resp = tf.reshape(resp, [-1, vars.grid_size, vars.grid_size, vars.b * 5])

    regr_loss *= resp
    #regr_loss =
    regr_loss = tf.reduce_sum(regr_loss, reduction_indices=(1, 2, 3))
    regr_loss = tf.reduce_mean(regr_loss)

    #pred_y = tf.argmax(output_c)
    eq = tf.equal(tf.argmax(output_c, 1), tf.cast(truth_c, tf.int64))
    eq = tf.cast(eq, tf.float32)
    eq *= truth_t
    acc = tf.reduce_sum(eq) / tf.reduce_sum(truth_t)

    softmax_loss *= truth_t
    softmax_loss = tf.reshape(softmax_loss, [-1, vars.grid_size * vars.grid_size])
    softmax_loss = tf.reduce_sum(softmax_loss, reduction_indices=1)
    loss = tf.reduce_mean(softmax_loss) + regr_loss

    global_step = tf.Variable(0, trainable=False)

    train_step = tf.train.AdamOptimizer(1e-3).minimize(loss, global_step=global_step)

    #acc = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(output_c, 1), tf.cast(truth_c, tf.int64)), tf.float32))

    return loss, train_step, acc, global_step

def conv2d(name, l_input, w, train, k=1):
    with tf.variable_scope(name) as scope:
        c = tf.nn.conv2d(l_input, w, strides=[1, k, k, 1], padding='SAME')

        bn = batch_norm(c, train)

    return tf.nn.elu(bn, name=name)

xic = tf.contrib.layers.xavier_initializer_conv2d

def compress(features, train):
    print(features)
    w = tf.get_variable(name="conv_comp", shape=[3, 3, vars.features_d, 512], initializer=xic())
    c = conv2d("compress", features, w, train)
    return c

class Model:
    def __init__(self):
        pass

    def inference(self, sess, train):
        #resnet = cnn_model.ResNet50(sess)
        resnet = cnn_model.VGGNet(sess)

        feature_ph = tf.placeholder_with_default(resnet.features, [None, vars.features_h, vars.features_w, vars.features_d])

        feature_ph = tf.stop_gradient(feature_ph)

        #feature_small = compress(feature_ph, train)

        output_c, output_r = predict(feature_ph, train)

        thing_t = tf.placeholder(tf.float32, [None, vars.grid_size, vars.grid_size])
        class_t = tf.placeholder(tf.float32, [None, vars.grid_size, vars.grid_size])
        resp_t = tf.placeholder(tf.float32, [None, vars.grid_size, vars.grid_size, vars.b * 5])
        reg_t = tf.placeholder(tf.float32, [None, vars.grid_size, vars.grid_size, vars.b * 5])

        loss, train_op, acc, global_step = trainer(output_c, output_r, thing_t, class_t, resp_t, reg_t)

        return resnet.images, resnet.features, [output_c, output_r], [thing_t, class_t, reg_t, resp_t], loss, train_op, acc, global_step
