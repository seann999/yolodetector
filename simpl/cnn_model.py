
import tensorflow as tf

class ResNet50:
    def __init__(self, sess):
        new_saver = tf.train.import_meta_graph("../tensorflow-resnet/tensorflow-resnet-pretrained-20160509/ResNet-L50.meta")
        new_saver.restore(sess, "../tensorflow-resnet/tensorflow-resnet-pretrained-20160509/ResNet-L50.ckpt")

        graph = tf.get_default_graph()
        self.images = graph.get_tensor_by_name("images:0")
        self.features = graph.get_operation_by_name("scale5/block3/Relu").values()[0]

class VGGNet:
    def __init__(self, sess):
        print("loading vgg...")

        with open("../vgg16-20160129.tfmodel", mode='rb') as f:
            fileContent = f.read()

        print("loaded vgg.")

        graph_def = tf.GraphDef()
        graph_def.ParseFromString(fileContent)

        self.images = tf.placeholder("float", [None, 224, 224, 3])

        tf.import_graph_def(graph_def, input_map={"images": self.images})

        graph = tf.get_default_graph()

        ops = sess.graph.get_operations()

        self.features = graph.get_tensor_by_name("import/pool5:0")