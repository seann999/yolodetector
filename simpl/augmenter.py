import numpy as np
import cv2
import scipy

def augment(img, info):
    #salt and pepper noise
    #blur
    #lighting

    fimg = img
    finfo = info
    #print(type(finfo))
   # print(img)

    if np.random.randint(0, 2) == 0:
        s = np.random.uniform(0.0, 0.3)
        s2 = np.random.uniform(0, 0.3)
        noise = np.random.uniform(-s, s2, size=img.shape)
        fimg += noise

    if np.random.randint(0, 2) == 0:
        s = np.random.uniform(0.0, 0.3)
        fimg += np.random.randn() * s

    if np.random.randint(0, 2) == 0:
        s = np.random.randint(1, 5) * 2 + 1
        fimg = cv2.GaussianBlur(fimg, (s, s), 0)

    if np.random.randint(0, 2) == 0:
        s = np.random.random_sample() * 0.3 + 0.7
        fimg = fimg * s

    if np.random.randint(0, 2) == 0:
        fimg = np.fliplr(fimg)

        for i in range(len(finfo)):
            x1, y1, x2, y2, id = finfo[i]
            finfo[i] = [1.0-x2, y1, 1.0-x1, y2, id]

    if np.random.randint(0, 2) == 0:
        offsets = np.random.uniform(0.0, 0.1, size=(4,))
        h, w = fimg.shape[:2]

        #print(fimg.shape)
        fimg = fimg[int(offsets[0]*h) : int((1.0-offsets[1])*h), int(offsets[2]*w) : int((1.0-offsets[3])*w), :]
        #print(fimg.shape)
        #print(fimg.shape)
        fimg = scipy.misc.imresize(fimg, size=(224,224))
        fimg = fimg / 255.0

        scaleH, scaleW = 1.0 - offsets[0] - offsets[1], 1.0 - offsets[2] - offsets[3]

        for i in range(len(finfo)):

            x1, y1, x2, y2, id = finfo[i]
            nx1 = (x1 - offsets[2]) / scaleW
            ny1 = (y1 - offsets[0]) / scaleH
            nx2 = (x2 - x1) / scaleW + nx1
            ny2 = (y2 - y1) / scaleH + ny1
            finfo[i] = [nx1, ny1, nx2, ny2, id]

    #finfo = np.asarray(finfo)
    #print(type(finfo))

    return fimg, finfo

